<?php

namespace Database\Factories;

use App\Models\Message;
use Illuminate\Database\Eloquent\Factories\Factory;
use Carbon\Carbon;

class MessageFactory extends Factory
{
    protected $model = Message::class;

    public function definition(): array
    {
    	return [
            'option' => 'APPLY_FOR_REPAIR',
            'name' => 'Jon Doe',
            'email' => 'testmail@testmail.com',
            'phoneNumber' => '123456789',
            'phoneCountry' => 'EU',
            'description' => 'iminution expression reasonable',
            'clientLanguage' => 'lv',
            'isRead' => 1,
            'isAnswered' => 1,
            'createdAt' => Carbon::now(),
            'editedAt' => Carbon::now(),
    	];
    }
}
