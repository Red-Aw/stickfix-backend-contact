<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMessagesTable extends Migration
{
    public function up()
    {
        Schema::create('messages', function (Blueprint $table) {
            $table->id();
            $table->enum('option', ['APPLY_FOR_REPAIR', 'APPLY_FOR_ITEM', 'QUESTION', 'OTHER',]);
            $table->string('name', 256);
            $table->string('email', 256);
            $table->string('phoneNumber', 32);
            $table->string('phoneCountry', 5);
            $table->text('description')->default('')->nullable();
            $table->string('clientLanguage')->default('lv');
            $table->boolean('isRead')->default(false);
            $table->boolean('isAnswered')->default(false);
            $table->timestamp('createdAt')->useCurrent();
            $table->timestamp('editedAt')->useCurrent();
        });
    }

    public function down()
    {
        Schema::dropIfExists('messages');
    }
}
