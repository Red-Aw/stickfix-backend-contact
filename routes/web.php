<?php

/** @var \Laravel\Lumen\Routing\Router $router */

$router->group(['prefix' => 'api'], function () use ($router) {
    $router->get('/getMessages', 'MessageController@getMessages');
    $router->get('/getMessage/{id}', 'MessageController@getMessage');
    $router->post('/createRequest', 'MessageController@store');
    $router->post('/createReply', 'MessageController@reply');
});
