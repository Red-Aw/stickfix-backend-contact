<?php

use App\Models\Message;
use App\Models\Reply;

class ContactTest extends TestCase
{
    public function testCreateRequestUnauthorized()
    {
        $messageData = [
            'name' => 'keely_schumm78',
            'email' => 'keely_schumm78@yahoo.com',
            'phone' => [
                'number' => '123456789',
                'country' => 'EU',
                'isValid' => 1,
            ],
            'description' => 'In it except to so temper mutual tastes mother. Interested cultivated its continuing now yet are. Out interested acceptance our partiality affronting unpleasant why add. Esteem garden men yet shy course. Consulted up my tolerably sometimes perpetual oh. Expression acceptance imprudence particular had eat unsatiable.',
            'radioOption' => 'APPLY_FOR_REPAIR',
            'language' => 'en',
        ];

        $this->json('POST', 'api/createRequest', $messageData, [ 'Accept' => 'application/json', 'Authorization' => '' ])
			->seeStatusCode(401);

        $this->clearServiceTableAtEnd();
    }

    public function testCreateRequest()
    {
        $messageData = [
            'name' => 'reziac',
            'email' => 'reziac@yahoo.com',
            'phone' => [
                'number' => '123456789',
                'country' => 'EU',
                'isValid' => 1,
            ],
            'description' => 'In it except to so temper mutual tastes mother. Interested cultivated its continuing now yet are. Out interested acceptance our partiality affronting unpleasant why add. Esteem garden men yet shy course. Consulted up my tolerably sometimes perpetual oh. Expression acceptance imprudence particular had eat unsatiable.',
            'radioOption' => 'APPLY_FOR_REPAIR',
            'language' => 'ru',
        ];

        $this->json('POST', 'api/createRequest', $messageData, [ 'Accept' => 'application/json', 'Authorization' => env('ALLOWED_SECRET') ])
            ->seeStatusCode(200)
            ->seeJsonContains([
                'success' => true,
            ]);

        $this->clearServiceTableAtEnd();
    }

	public function testCreateReplyUnauthorized()
    {
        $messageData = [
            'option' => 'APPLY_FOR_REPAIR',
            'name' => 'cyril_kozey',
            'email' => 'cyril_kozey@yahoo.com',
            'phoneNumber' => '123456789',
            'phoneCountry' => 'EU',
            'description' => 'He moonlight difficult engrossed an it sportsmen. Interested has all devonshire difficulty gay assistance joy.',
            'clientLanguage' => 'lv',
        ];

        $messageObj = \App\Models\Message::factory()->create($messageData);

        $replyData = [
            'id' => $messageObj->id,
            'newReplyText' => 'Removed demands expense account in outward tedious do. Particular way thoroughly unaffected projection favourable mrs can projecting own',
        ];

        $this->json('POST', 'api/createReply', $replyData, [ 'Accept' => 'application/json', 'Authorization' => '' ])
            ->seeStatusCode(401);

        $this->clearServiceTableAtEnd();
    }

    public function testCreateReply()
    {
        $messageData = [
            'option' => 'QUESTION',
            'name' => 'lew_nader',
            'email' => 'lew_nader@yahoo.com',
            'phoneNumber' => '123456789',
            'phoneCountry' => 'EU',
            'description' => 'offices parties lasting outward nothing age few resolve. Impression to discretion understood to we interested he excellence',
            'clientLanguage' => 'ru',
        ];

        $messageObj = \App\Models\Message::factory()->create($messageData);

        $replyData = [
            'id' => $messageObj->id,
            'newReplyText' => 'Nor hence hoped her after other known defer his. For county now sister engage had season better had waited.',
        ];

        $this->json('POST', 'api/createReply', $replyData, [ 'Accept' => 'application/json', 'Authorization' => env('ALLOWED_SECRET') ])
            ->seeStatusCode(200)
            ->seeJsonContains([
                'success' => true,
            ]);

        $this->clearServiceTableAtEnd();
    }

    public function testGetMessagesUnauthorized()
    {
        $firstMessageData = [
            'option' => 'OTHER',
            'name' => 'karli_okuneva85',
            'email' => 'karli_okuneva85@yahoo.com',
            'phoneNumber' => '123456789',
            'phoneCountry' => 'EU',
            'description' => 'Prospect humoured mistress to by proposal marianne attended. Simplicity the far admiration preference everything. Up help home head spot an he room in',
            'clientLanguage' => 'en',
        ];
        \App\Models\Message::factory()->create($firstMessageData);

        $secondMessageData = [
            'option' => 'OTHER',
            'name' => 'demond12',
            'email' => 'demond12@hotmail.com',
            'phoneNumber' => '123456789',
            'phoneCountry' => 'EU',
            'description' => 'onsidered an invitation do introduced sufficient understood instrument it. Of decisively friendship in as collecting at',
            'clientLanguage' => 'lv',
        ];
        \App\Models\Message::factory()->create($secondMessageData);

        $this->json('GET', 'api/getMessages', [], [ 'Accept' => 'application/json', 'Authorization' => '' ])
            ->seeStatusCode(401);

        $this->clearServiceTableAtEnd();
    }

    public function testGetMessages()
    {
        $firstMessageData = [
            'option' => 'APPLY_FOR_REPAIR',
            'name' => 'cyril_kozey',
            'email' => 'cyril_kozey@yahoo.com',
            'phoneNumber' => '123456789',
            'phoneCountry' => 'EU',
            'description' => 'He moonlight difficult engrossed an it sportsmen. Interested has all devonshire difficulty gay assistance joy.',
            'clientLanguage' => 'en',
        ];
        \App\Models\Message::factory()->create($firstMessageData);

        $secondMessageData = [
            'option' => 'APPLY_FOR_ITEM',
            'name' => 'santiago22',
            'email' => 'santiago22@yahoo.com',
            'phoneNumber' => '123456789',
            'phoneCountry' => 'EU',
            'description' => 'Unaffected at ye of compliment alteration to. Place voice no arise along to. Parlors waiting so against me no.',
            'clientLanguage' => 'ru',
        ];
        \App\Models\Message::factory()->create($secondMessageData);

        $this->json('GET', 'api/getMessages', [], [ 'Accept' => 'application/json', 'Authorization' => env('ALLOWED_SECRET') ])
            ->seeStatusCode(200)
            ->seeJsonContains(['success' => true]);

        $this->clearServiceTableAtEnd();
    }

    public function testGetMessageUnauthorized()
    {
        $messageData = [
            'option' => 'APPLY_FOR_ITEM',
            'name' => 'tiana34',
            'email' => 'tiana34@gmail.com',
            'phoneNumber' => '123456789',
            'phoneCountry' => 'EU',
            'description' => 'Greatly hearted has who believe. Drift allow green son walls years for blush. Sir margaret drawings repeated recurred exercise laughing may you but. Do repeated whatever to welcomed absolute no. Fat surprise although outlived and informed shy dissuade property. Musical by me through he drawing savings an. No we stand avoid decay heard mr. Common so wicket appear to sudden worthy on. Shade of offer ye whole stood hoped.',
            'clientLanguage' => 'lv',
        ];

        $messageObj = \App\Models\Message::factory()->create($messageData);

        $this->json('GET', 'api/getMessage/' . $messageObj->id, [], [ 'Accept' => 'application/json', 'Authorization' => '' ])
            ->seeStatusCode(401);

        $this->clearServiceTableAtEnd();
    }

    public function testGetMessage()
    {
        $messageData = [
            'option' => 'QUESTION',
            'name' => 'breana_sauer81',
            'email' => 'breana_sauer81@gmail.com',
            'phoneNumber' => '123456789',
            'phoneCountry' => 'EU',
            'description' => 'It chicken oh colonel pressed excited suppose to shortly. He improve started no we manners however effects',
            'clientLanguage' => 'en',
        ];

        $messageObj = \App\Models\Message::factory()->create($messageData);

        $this->json('GET', 'api/getMessage/' . $messageObj->id, [], [ 'Accept' => 'application/json', 'Authorization' => env('ALLOWED_SECRET') ])
            ->seeStatusCode(200)
            ->seeJsonContains([
                'success' => true,
            ]);

        $this->clearServiceTableAtEnd();
    }

    private function clearServiceTableAtEnd()
    {
        Message::truncate();
        Reply::truncate();
    }
}
