<p><b>Do not reply to this message!</b></p>
<p><b>Reply to credentials provided below</b></p>

<hr>

<p>Izvēle: {{ $data['messageOption'] }}</p>
<p>Sūtītājs: {{ $data['senderName'] }}</p>
<p>Epasts: {{ $data['senderEmail'] }}</p>
<p>Telefona numurs: {{ $data['senderPhone'] }}</p>
<p>Teksts: {{ $data['messageDescription'] }}</p>
