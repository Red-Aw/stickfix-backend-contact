<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class NewReply extends Mailable
{
    use Queueable, SerializesModels;

    public function __construct($data)
    {
        $this->data = $data;
    }

    public function build()
    {
        return $this->view('emails.newReply')
            ->to($this->data['to'], $this->data['senderName'])
            ->subject('#' . $this->data['messageId'] . ' ' . $this->data['messageOption'] . ' - SIA StickFix')
            ->from(env('MAIL_FROM_ADDRESS'), env('MAIL_FROM_NAME'))
            ->with('data', $this->data);
    }
}
