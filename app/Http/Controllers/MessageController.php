<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Mail;

use Illuminate\Database\Eloquent\ModelNotFoundException;

use App\Http\Controllers\EnumController;

use App\Models\Message;
use App\Models\Reply;

use App\Mail\NewReply;
use App\Mail\NewRequest;

use Carbon\Carbon;

class MessageController extends Controller
{
    private $enumController;

    public function __construct(EnumController $enumController)
    {
        $this->enumController = $enumController;
    }

    const MESSAGE_OPTIONS = array(
        'APPLY_FOR_REPAIR' => array(
            'lv' => 'Pieteikt remontu',
            'en' => 'Apply for repair',
            'ru' => 'Подать заявку на ремонт',
        ),
        'APPLY_FOR_ITEM' => array(
            'lv' => 'Pieteikt preci',
            'en' => 'Apply for the product',
            'ru' => 'Подать заявку на продукт',
        ),
        'QUESTION' => array(
            'lv' => 'Jautājums',
            'en' => 'Question',
            'ru' => 'Вопрос',
        ),
        'OTHER' => array(
            'lv' => 'Cits',
            'en' => 'Other',
            'ru' => 'Другой',
        ),
    );

    public function getMessages()
    {
        try {
            $messages = Message::orderBy('createdAt', 'desc')->get();
        } catch (\Exception $e) {
            return $this->response(false, 'error.errorSelectingData', [], Response::HTTP_OK, null);
        }

        return $this->response(true, '', ['messages' => $messages], Response::HTTP_OK, null);
    }

    public function getMessage($id)
    {
        try {
            $messageData = Message::with('replies')->findOrFail($id);

            if (!$messageData->isRead) {
                $dateTime = Carbon::now();
                $messageData->isRead = true;
                $messageData->editedAt = $dateTime->toDateTimeString();
                $updated = $messageData->save();

                if (!$updated) {
                    return $this->response(false, 'error.databaseError', [], Response::HTTP_OK, null);
                }
            }
        } catch (ModelNotFoundException $e) {
            return $this->response(false, 'error.recordNotFound', [], Response::HTTP_OK, null);
        }

        return $this->response(true, '', ['message' => $messageData], Response::HTTP_OK, null);
    }

    public function store(Request $request)
    {
        $rules = [
            'name' => 'required|min:3|max:256',
            'email' => 'required|email|max:256',
            'phone.number' => 'required|min:3|max:32',
            'phone.country' => 'required|min:1|max:5',
            'phone.isValid' => 'boolean',
            'description' => 'max:4096',
            'radioOption' => 'required',
            'language' => 'required',
        ];

        $messages = [
            'name.required' => 'validation.requiredField',
            'name.min' => 'validation.mustBeAtLeast3CharsLong',
            'name.max' => 'validation.mustBe256CharsOrFewer',
            'email.required' => 'validation.requiredField',
            'email.email' => 'validation.invalidEmail',
            'email.max' => 'validation.mustBe256CharsOrFewer',
            'phone.number.required' => 'validation.requiredField',
            'phone.number.min' => 'validation.mustBeAtLeast3CharsLong',
            'phone.number.max' => 'validation.mustBe32CharsOrFewer',
            'phone.country.required' => 'validation.requiredField',
            'phone.country.min' => 'validation.mustBeAtLeast1CharsLong',
            'phone.country.max' => 'validation.mustBe5CharsOrFewer',
            'phone.isValid.boolean' => 'validation.requiredField',
            'description.max' => 'validation.mustBe4096CharsOrFewer',
            'language.required' => 'validation.requiredField',
            'radioOption.required' => 'validation.requiredField',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return $this->response(false, 'error.validationError', [], Response::HTTP_OK, $validator->errors());
        }

        if (!$this->isValidMessageOption($request->radioOption)) {
            return $this->response(
                false,
                'error.validationError',
                [],
                Response::HTTP_OK,
                [ 'radioOption' => ['validation.requiredField'] ],
            );
        }

        $message = new Message();
        $message->option = $request->radioOption;
        $message->name = $request->name;
        $message->email = $request->email;
        $message->phoneNumber = $request->phone['number'];
        $message->phoneCountry = $request->phone['country'];
        $message->description = $request->description;
        $message->clientLanguage = $request->language;
        $created = $message->save();

        if (!$created) {
            return $this->response(false, 'error.databaseError', [], Response::HTTP_OK, null);
        }

        $data = [
            'messageId' => $message->id,
            'messageOption' => self::MESSAGE_OPTIONS[$message->option]['lv'],
            'senderName' => $message->name,
            'senderEmail' => $message->email,
            'senderPhone' => $message->phoneNumber,
            'messageDescription' => $message->description,
        ];

        $success = $this->sendEmail(new NewRequest($data));
        if (!$success) {
            $message->delete();
            return $this->response(false, 'error.messageNotSent', [], Response::HTTP_OK, null);
        }

        return $this->response(true, 'messageSent', [], Response::HTTP_OK, null);
    }

    public function reply(Request $request)
    {
        $rules = [
            'id' => 'required|exists:messages',
            'newReplyText' => 'required|min:3|max:4096',
        ];

        $messages = [
            'id.required' => 'validation.identifierDoesNotExist',
            'id.exists' => 'validation.identifierDoesNotExist',
            'newReplyText.required' => 'validation.requiredField',
            'newReplyText.min' => 'validation.mustBeAtLeast3CharsLong',
            'newReplyText.max' => 'validation.mustBe4096CharsOrFewer',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return $this->response(false, 'error.validationError', [], Response::HTTP_OK, $validator->errors());
        }

        $dateTime = Carbon::now();

        $message = Message::find($request->id);
        if (!$message->isAnswered) {
            $message->isAnswered = true;
            $message->editedAt = $dateTime->toDateTimeString();
            $updated = $message->save();

            if (!$updated) {
                return $this->response(false, 'error.databaseError', [], Response::HTTP_OK, null);
            }
        }

        $reply = new Reply();
        $reply->messageId = $message->id;
        $reply->description = $request->newReplyText;
        $created = $reply->save();

        if (!$created) {
            return $this->response(false, 'error.databaseError', [], Response::HTTP_OK, null);
        }

        $data = [
            'to' => $message->email,
            'senderName' => $message->name,
            'messageId' => $message->id,
            'messageOption' => self::MESSAGE_OPTIONS[$message->option][$message->clientLanguage],
            'messageDescription' => $message->description,
            'replyDescription' => $reply->description,
        ];

        $success = $this->sendEmail(new NewReply($data));
        if (!$success) {
            $reply->delete();
            return $this->response(false, 'error.messageNotSent', [], Response::HTTP_OK, null);
        }

        return $this->response(true, 'messageSent', [], Response::HTTP_OK, null);
    }

    public function sendEmail($mailTemplate)
    {
        $success = true;

        if (env('APP_ENV') !== 'production') {
            return $success;
        }

        try {
            Mail::send($mailTemplate);
        } catch(\Exception $e) {
            $success = false;
        }

        if (Mail::failures()) {
            $success = false;
        }

        return $success;
    }

    private function isValidMessageOption($chosenOption)
    {
        foreach ($this->enumController->getEnums('MESSAGE_OPTIONS') as $option) {
            if ($chosenOption === $option) {
                return true;
            }
        }

        return false;
    }
}
