<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    use HasFactory;

    public $timestamps = false;

    protected $fillable = [
        'option',
        'name',
        'email',
        'phoneNumber',
        'phoneCountry',
        'description',
        'clientLanguage',
        'isRead',
        'isAnswered',
        'createdAt',
        'editedAt',
    ];

    public function replies()
    {
        return $this->hasMany(Reply::class, 'messageId');
    }
}
