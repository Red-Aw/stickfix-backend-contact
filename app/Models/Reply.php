<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Reply extends Model
{
    use HasFactory;

    public $timestamps = false;

    protected $fillable = [
        'messageId',
        'description',
        'createdAt',
    ];

    public function message()
    {
        $this->belongsTo(Message::class, 'id');
    }
}
