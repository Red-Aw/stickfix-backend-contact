<?php

return [
    'enums' => [
        'baseUri' => env('ENUMS_SERVICE_BASE_URI'),
        'secret' => env('ENUMS_SERVICE_SECRET'),
    ],
];